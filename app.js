var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors')
const compression = require('compression');
global.APP_PATH = path.resolve(__dirname);
var router = express.Router();
var app = express();
app.use(compression());
var Data;

const mysql = require('mysql2');
const Sequelize = require('sequelize');

function initiateUser(){
	Data = sequelize.define('data', {
	  name: {
	    type: Sequelize.STRING
	  }
	});

}

const sequelize = new Sequelize('testPackage', 'root', '12345', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },

});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
    initiateUser()
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/add/:item', function(req,res){
	if(req.params && req.params.item){

		Data.create({
		    name: req.params.item
		});

		return res.json({body:{status:1,message:"Stored Successfully"}});
	}else{
		return res.json({body:{status:0,message:"Please send the text"}});
	}
});




app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
  next();
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

module.exports = app;
